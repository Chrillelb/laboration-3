package se.hig.aod.lab3.main;

public class FullQueueException extends RuntimeException {

	/**
	 * @author Christian Bergqvist
	 * @version 2020-12-08
	 */
	private static final long serialVersionUID = 1L;

	public FullQueueException() {
		super();
	}

	public FullQueueException(String message) {
		super(message);
	}

	public FullQueueException(String message, Throwable cause) {
		super(message, cause);
	}

	public FullQueueException(Throwable cause) {
		super(cause);
	}

	protected FullQueueException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}