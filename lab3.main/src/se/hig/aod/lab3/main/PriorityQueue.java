package se.hig.aod.lab3.main;

public interface PriorityQueue<T extends Comparable<? super T>> {

	/**
	 * @author Christian Bergqvist
	 * @version 2020-12-08
	 */

	/**
	 * Clear queue of elements
	 */
	void clear();

	/**
	 * Checks if empty
	 * 
	 * @return true or false
	 */
	boolean isEmpty();

	/**
	 * Checks if full
	 * 
	 * @return true or false
	 */
	boolean isFull();

	/**
	 * 
	 * Returns nbr of elements in queue
	 */
	int size();

	/**
	 * Adds element into queue * Will throw {@link FullQueueException} if queue full
	 * 
	 * @param element
	 */
	void enqueue(T element);

	/**
	 * * Will throw {@link QueueEmptyException} if queue is empty Removes the first
	 * element, highest priority
	 * 
	 * @return element
	 */
	T dequeue();

	/**
	 * Will throw {@link QueueEmptyException} if queue is empty Get first element,
	 * highest priority without removing it
	 * 
	 * @return element
	 */
	T getFront();

	String printQueue();

}
