package se.hig.aod.lab3.main;

/**
 * @author Christian Bergqvist
 * @version 2020-12-08
 */

public class HeapPriorityQueue<T extends Comparable<? super T>> implements PriorityQueue<T> {

	private Object[] array;
	private int maxSize;
	private int queueSize;

	public HeapPriorityQueue(int maxSize) {
		this.maxSize = maxSize;
		array = new Object[maxSize];
	}

	@Override
	public void clear() {
		queueSize = 0;
	}

	@Override
	public boolean isEmpty() {
		return (queueSize == 0);
	}

	@Override
	public boolean isFull() {
		return (queueSize == maxSize);
	}

	@Override
	public int size() {
		return queueSize;
	}
	
	//Enqueue s�tter in ett element l�ngst ner och percolateUP bubblar upp 
	//elementet till r�tt plats 
	@Override
	public void enqueue(T element) {
		if (isFull()) {
			throw new FullQueueException();
		} else {
			array[queueSize] = element;
			queueSize++;
			percolateUp();
		}
	}
	
	
	//dequeue tar bort element l�ngst upp(h�gst prio) 
	// nedersta element flyttas till top, percolateDown bubblar ner till r�tt plats
	@SuppressWarnings("unchecked")
	@Override
	public T dequeue() {
		if (isEmpty()) {
			throw new EmptyQueueException();
		} else {
			T element = (T) array[0];
			array[0] = null;
			percolateDown();
			queueSize--;
			return element;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public T getFront() {
		if (isEmpty()) {
			throw new EmptyQueueException();
		} else {
			return (T) array[0];
		}
	}

	private void percolateUp() {
		int k = queueSize - 1;
		// tar reda p� index till n�sta parent nod
		int parent = (k - 1) / 2;
		// fors�tter till roten och medan k �r mindre �n parent
		while (k > 0 && lesser(k, parent)) {
			// Byt plats p� k och parent med swap metod.
			swap(parent, k);
			k = parent;
			parent = (k - 1) / 2;
		}
	}

	private void percolateDown() {

		int heapSize = queueSize;
		int k = queueSize - 1;
		for (int i = 0; i <= queueSize; i++) {
			int left = 2 * k + 1; // v�nstra noden
			int right = 2 * k + 2; // h�gra noden
			int smallest = left; // antar att den v�nstra noden �r den minsta
			// Tar reda vilken som �r "st�rst
			if (right < heapSize && lesser(right, left))
				smallest = right;
			// stannar om vi inte kan g� l�ngre ned i tr�d-strukturen
			if (left >= heapSize || lesser(k, smallest))
				break;
			// Flytta "ner" tr�det s� att den minsta hamnar �verst
			swap(smallest, k);
			k = smallest;
		}
	}

	/**
	 * Hj�lp metod f�r att j�mf�ra 2 tal
	 * 
	 * @param i
	 * @param j
	 * @return -+0
	 */
	@SuppressWarnings("unchecked")
	private boolean lesser(int i, int j) {
		T node1 = (T) array[i];
		T node2 = (T) array[j];
		return node1.compareTo(node2) <= 0;
	}

	/**
	 * Hj�lp metod f�r att byta plats p� tv� noder
	 * 
	 * @param i
	 * @param j
	 */
	@SuppressWarnings("unchecked")
	private void swap(int i, int j) {
		T node1 = (T) array[i];
		T node2 = (T) array[j];
		array[i] = node2;
		array[j] = node1;
	}

	public String printQueue() {
		String arr = "";
		for (int i = 0; i < queueSize; i++) {
			arr = arr + array[i].toString() + ", ";
		}
		return arr;
	}
}
