package se.hig.aod.lab3.main;

/**
 * @author Christian Bergqvist 
 * @version 2020-12-08
 */
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class HeapPriorityQueueTest {

	PriorityQueue<Integer> pQueue;

	/**
	 * Create Queue.
	 */
	@BeforeEach
	void setUp() {
		pQueue = new HeapPriorityQueue<Integer>(3);
	}

	/**
	 * Destroy Queue.
	 */
	@AfterEach
	void tearDown() {
		pQueue = null;
	}

	/**
	 * Test clear after enqueue.
	 */
	@Test
	void testClear() {
		pQueue.enqueue(1);
		pQueue.clear();
		assertTrue(pQueue.isEmpty());

	}

	/**
	 * Test clear on empty Queue.
	 */
	@Test
	void testClearIfEmpty() {
		assertTrue(pQueue.isEmpty());
		pQueue.clear();
		assertTrue(pQueue.isEmpty());
	}

	/**
	 * Test if Queue is empty.
	 */
	@Test
	void isEmptyTest() {
		pQueue.clear();
		assertTrue(pQueue.isEmpty());
		pQueue.enqueue(1);
		assertFalse(pQueue.isEmpty());
	}

	/**
	 * Test if Queue is empty on birth.
	 */
	@Test
	void isEmptyTestFromStart() {
		assertTrue(pQueue.isEmpty());
	}

	/**
	 * Test if full after max enqueue.
	 */
	@Test
	void isFullTest() {
		assertFalse(pQueue.isFull());
		pQueue.enqueue(1);
		pQueue.enqueue(2);
		pQueue.enqueue(3);
		assertTrue(pQueue.isFull());
	}

	/**
	 * Checks the size.
	 */
	@Test
	void sizeTest() {
		pQueue.enqueue(1);
		pQueue.enqueue(2);
		pQueue.enqueue(3);
		assertEquals(3, pQueue.size());
	}

	/**
	 * Checks if size have correct behavior multiple items.
	 */
	@Test
	void sizeChangeTest() {
		assertEquals(0, pQueue.size());
		pQueue.enqueue(1);
		assertEquals(1, pQueue.size());
		pQueue.dequeue();
		assertEquals(0, pQueue.size());
	}

	/**
	 * Test to enqueue, checks size and if the correct item is prioritized.
	 */
	@Test
	void enqueueTest() {
		pQueue.enqueue(3);
		pQueue.enqueue(1);
		pQueue.enqueue(2);
		assertEquals(3, pQueue.size());
		assertEquals(1, pQueue.dequeue());
	}

	/**
	 * Test to enqueu negative nbr, checks if correct item is prioritized.
	 */
	@Test
	void enqueueTestNegativeNbr() {
		pQueue.enqueue(-2);
		pQueue.enqueue(-7000);
		pQueue.enqueue(0);
		assertEquals(-7000, pQueue.dequeue());
	}

	/**
	 * Test to enqueu same nbr, checks if correct item is prioritized :).
	 */
	@Test
	void enqueueTestSameNbr() {
		pQueue.enqueue(5);
		pQueue.enqueue(5);
		pQueue.enqueue(5);
		assertEquals(5, pQueue.getFront());
	}

	/**
	 * Test if correct exception is thrown when enqueue on full Queue.
	 */
	@Test
	void enqueueOnFullExceptionTest() {
		pQueue.enqueue(3);
		pQueue.enqueue(1);
		pQueue.enqueue(2);
		assertThrows(FullQueueException.class, () -> pQueue.enqueue(4), "Expected: FullQueueException");
	}

	/**
	 * Test to dequeue,size change and correct item is prioritized.
	 */
	@Test
	void dequeueTest() {
		pQueue.enqueue(1);
		pQueue.enqueue(2);
		pQueue.enqueue(3);
		assertEquals(3, pQueue.size());
		assertEquals(1, pQueue.dequeue());
		pQueue.clear();
		assertEquals(0, pQueue.size());

	}

	/**
	 * Test to dequeue on empty Queue and if correct exception is thrown.
	 */

	@Test
	void dequeueExceptionOnEmptyQueueTest() {
		assertTrue(pQueue.isEmpty());
		assertThrows(EmptyQueueException.class, () -> pQueue.dequeue(), "Expected: EmptyQueueException");
	}

	/**
	 * Test to get front and size stays the same.
	 */
	@Test
	void getFrontTest() {
		pQueue.enqueue(1);
		pQueue.enqueue(2);
		pQueue.enqueue(3);
		assertEquals(3, pQueue.size());
		assertEquals(1, pQueue.getFront());
		assertEquals(3, pQueue.size());

	}

	/**
	 * Test to getFront on empty Queue and if correct exception is thrown.
	 */
	@Test
	void getFrontIfEmptyExceptionTest() {
		assertTrue(pQueue.isEmpty());
		assertThrows(EmptyQueueException.class, () -> pQueue.getFront(), "Expected: EmptyQueueException");
	}

	/**
	 * Test to print items and that the correct item i prioritized.
	 */
	@Test
	void printTest() {
		pQueue.enqueue(2000);
		pQueue.enqueue(1000);
		pQueue.enqueue(1);
		System.out.println(pQueue.printQueue());
		pQueue.clear();
		pQueue.enqueue(10000);
		pQueue.enqueue(9999);
		pQueue.enqueue(10001);
		System.out.println(pQueue.printQueue());

	}

}
