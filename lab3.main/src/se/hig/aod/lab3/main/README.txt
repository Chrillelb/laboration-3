
Laboration 3 - algoritmer & datastrukturer 
H�gskolan i G�vle, HT 2020

Av: Christian Bergqvist 

Uppgift 1:
Inte klar 

Uppgift 2: 
I den andra uppgiften skapas ett interface f�r en prioritetsk� 
enligt labinstruktioner. Med metoderna: clear,isEmpty, isFull,size, enqueue, dequeue, getfront. 
Se "PriorityQueue.java".


Uppgift 3:
I den tredje uppgiften skapas ett J Unit Test Case som testar all funktionalitet i det nyligen 
skapade interfacet PriorityQueue.java. Se HeapPriorityQueueTest.java

Uppgift 4: 
I den fj�rde uppgiften f�r man v�lja om man vill skapa en Heap eller Bst, i detta fall v�ljs Heap. 
En ny klass HeapPriorityQueue.java skapas som implementerar interfacet ifr�n uppgift 2. 



Diskussionsfr�gor

1. Hur b�r prioritetsk�n hantera eventuella dubbletter?
En prioritetsk� borde hantera samma proritet enligt FIFO allts� att de kommer ut som de kom in i k�n. 

2. Vilken �r tids�tg�ngen i v�rsta fallet respektive �genomsnittsfallet� f�r enqueue-operationen
om vi anv�nder:
	(a)en sorterad listbaserad implementation?
	
Svar: O(n) �r v�rsta(g� igenom alla element i listan) och 0(n)/2 (O(n)) �r genomsnitts(h�lften av elementen)

	(b)en BST-baserad implementation?
	
Svar:Det v�rsta �r att g� igenom hela tr�det dvs tr�dets h�jd. O(n) urartad lista average: O (log n) (/2) 

	(c) en heap-baserad implementation?
Svar: O(log N) tid d�r N �r antalet noder average: samma  

3. Vilken �r tids�tg�ngen i v�rsta fallet respektive �genomsnittsfallet� f�r dequeue-operationen
om vi anv�nder:
	(a)en sorterad listbaserad implementation?
	O(1) spelar ingen roll om sorterad eller inte 
	(b)en BST-baserad implementation?
	
	Svar: O(n) pga urartad lista     v�rsta fallet O(n)
	
	(c) en heap-baserad implementation?
	
Svar: O(logn) p� b�da.


4: 

