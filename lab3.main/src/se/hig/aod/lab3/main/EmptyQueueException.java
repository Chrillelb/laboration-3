package se.hig.aod.lab3.main;

/**
 * @author Christian Bergqvist
 * @version 2020-12-08
 */

public class EmptyQueueException extends RuntimeException {
	static final long serialVersionUID = 1;

	public EmptyQueueException() {
		super();
	}

	public EmptyQueueException(String message) {
		super(message);
	}

	public EmptyQueueException(String message, Throwable cause) {
		super(message, cause);
	}

	public EmptyQueueException(Throwable cause) {
		super(cause);
	}

	protected EmptyQueueException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
